var mongoose = require('mongoose')
const User = require('../models/userModel')

exports.user_list = function (req, res) {
    // User.find({}, function (err, result) {
    //     if (err) {
    //         res.json({
    //             status: 404,
    //             message: 'Data not found'
    //         })
    //     } else {
    //         var data = result;
    //         res.json({
    //             status: 200,
    //             data: data
    //         })
    //     }
    // })
    User.find({}).populate('roleId')
        .exec(function (err, result) {
            if (err) return res.status(500).send('Internal Server Error')
            if (!result) return res.status(404).send('Data Not Found')

            var data = result
            return res.status(200).json({
                data: data
            })
        })
}

exports.create_user = function (req, res) {
    let new_user = new User(req.body)
    new_user.save(function (err, result) {
        if (err) return res.status(500).send('Internal Server Error')
        return res.json(result)
    })
}

exports.update_user = function (req, res) {
    User.findByIdAndUpdate(req.query.id, req.body, {
        new: true
    }, function (err, result) {
        if (err) return res.status(500).send('Internal Server Error')

        return res.json(result)
    })
}

exports.delete_user = function (req, res) {
    User.findByIdAndDelete(req.query.id, function (err, result) {
        if (err) return res.status(500).send('Internal Server Error')
        return res.json({
            status: 200,
            message: 'User Successfully Deleted'
        })
    })
}

exports.user_by_id = function (req, res, next) {
    User.findById(req.query.id, function (err, result) {
        if (err) return res.status(500).send('Internal Server Error')
        if (!result) return res.status(404).send('User Not Found')

        var data = result
        return res.json({
            data: data
        })
    })
}