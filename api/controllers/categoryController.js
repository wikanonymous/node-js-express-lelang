var Category = require('../models/categoryModel')

exports.create_category = function (req, res) {
    let new_category = new Category(req.body)

    new_category.save(function (err, result) {
        if (err) {
            return res.status(500).send('Internal Server Error')
        }
        var data = result
        return res.json({
            status: 200,
            data: data
        })
    })
}

exports.category_list = function (req, res) {
    Category.find({}, function (err, result) {
        if (err) {
            return res.status(500).send('Internal Server Error')
        } else {
            var data = result;
            return res.json({
                status: 200,
                data: data
            })
        }
    })
}