var Loan = require('../models/loanModel')

exports.loan_register = function (req, res) {

    Loan.find({
        loanId: req.body.loanId
    }, function (err, result) {
        if (err) return res.status(500).send('Internal Server Error')
        var data = {
            count: result.length
        }

        if (data.count == 0) data.count = 1
        else data.count += 1

        Loan.create({
            loanId: req.body.loanId,
            userId: req.body.userId,
            counter: data.count
        }, (err, result) => {
            if (err) return res.status(500).send('Internal Server Error')

            var loan = result
            return res.json({
                status: 200,
                message: 'You have successfully register to loan',
                data: loan
            })
        })

    })

    // Loan.findOneAndUpdate({
    //     loanId: req.body.loanId
    // }, {
    //     $inc: {
    //         counter: 1
    //     }
    // }, function (err, result) {
    //     if (err) return res.status(500).send('Internal Server Error')
    // })
}

exports.loan_list = function (req, res) {
    Loan.find({})
        .populate('loanId')
        .populate('userId')
        .exec(function (err, result) {
            if (err) return res.status(500).send('Internal Server Error')
            if (!result) return res.status(404).send('Data Not Found')

            var data = result
            return res.status(200).json({
                data: data
            })
        })
}

exports.loan_list_by_userId = function (req, res) {
    Loan.find({
            userId: req.query.userId
        })
        .populate({
            path: 'loanId'
        })
        .exec(function (err, result) {
            if (err) return res.status(500).send('Internal Server Error')
            if (!result) return res.status(404).send('Data Not Found')

            var data = result

            return res.status(200).json({
                data: data
            })
        })
}

exports.check_counter = function (req, res) {
    Loan.findOne({
        loanId: req.query.loanId,
        userId: req.query.userId
    }, (err, result) => {
        if (err) return res.status(500).send('Internal Server Error')

        res.json({
            status: 200,
            data: result
        })
    })
}