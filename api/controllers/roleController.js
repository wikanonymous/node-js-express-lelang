var mongoose = require('mongoose')
const Role = require('../models/roleModel')

exports.role_list = function (req, res) {
    Role.find({}, function (err, result) {
        if (err) {
            return res.status(500).send('Internal Server Error')
        }
        return res.json(result)
    })
}

exports.create_role = function (req, res) {
    let new_role = new Role(req.body)

    new_role.save(function (err, result) {
        if (err) {
            return res.status(500).send('Internal Server Error')
        }
        return res.json(result)
    })
}

exports.delete_role = function (req, res) {
    Role.findOneAndDelete(req.params.id, function (err, result) {
        if (err) {
            return res.status(500).send('Internal Server Error')
        }
        return res.json({
            status: 200,
            message: 'Successfully Delete Role'
        })
    })
}