var Status = require('../models/statusModel')

exports.status_list = function (req, res) {
    Status.find({}, function (err, result) {
        if (err) {
            return res.status(500).send('Internal Server Error')
        }
        return res.json(result)
    })
}

exports.create_status = function (req, res) {
    let new_status = new Status(req.body)

    new_status.save(function (err, result) {
        if (err) {
            return res.status(500).send('Internal Server Error')
        }
        return res.json(result)
    })
}

exports.update_status = function (req, res) {
    Status.findOneAndUpdate(req.params.id, req.body, {
        new: true
    }, function (err, result) {
        if (err) {
            return res.status(500).send('Internal Server Error')
        }
        return res.json(result)
    })
}

exports.delete_status = function (req, res) {
    Status.findOneAndDelete(req.params.id, function (err, result) {
        if (err) {
            return res.status(500).send('Internal Server Error')
        }
        return res.json(result)
    })
}