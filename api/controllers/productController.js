var Product = require('../models/productModel')
var Loan = require('../models/loanModel')

exports.create_product = function (req, res) {
    // let new_product = new Product(req.body)

    // new_product.save(function (err, result) {
    //     if (err) return res.status(500).send('Internal Server Error')

    //     var data = result;
    //     res.json({
    //         status: 200,
    //         message: 'You have successfully create a product',
    //         data: data
    //     })
    // })

    Product.create({
        categoryId: req.body.categoryId,
        owner: req.body.owner,
        name: req.body.name,
        license: req.body.license,
        tax: req.body.tax,
        created_year: req.body.created_year,
        color: req.body.color,
        price: req.body.price,
        start_event: req.body.start_event,
        end_event: req.body.end_event,
        winner: req.body.owner
    }, (err, result) => {
        if (err) return res.status(500).send('Internal Server Error')

        var data = result
        res.json({
            status: 200,
            message: 'You have successfully create a loan',
            data: data
        })
    })
}

exports.list_product = function (req, res) {
    // Product.find({}, function (err, result) {
    //     if (err) {
    //         return res.status(500).send('Internal Server Error')
    //     } else {
    //         var data = result;
    //         res.json({
    //             status: 200,
    //             data: data
    //         })
    //     }
    // })
    Product.find({})
        .populate('categoryId')
        .populate('owner')
        .populate('statusId')
        .exec(function (err, result) {
            if (err) return res.status(500).send('Internal Server Error')
            if (!result) return res.status(404).send('Data Not Found')

            var data = result
            res.status(200).json({
                data: data
            })
        })
}

exports.update_product = function (req, res) {
    Product.findByIdAndUpdate(req.query.id, req.body, {
        new: true
    }, function (err, result) {
        if (err) return res.status(500).send('Internal Server Error')

        res.json({
            status: 200,
            message: 'You have successfully update the data'
        })
    })
}

exports.delete_product = function (req, res) {
    Product.findByIdAndDelete(req.query.id, function (err, result) {
        if (err) return res.status(500).send('Internal Server Error')
        res.status(200).json({
            message: 'You have successfully delete the data'
        })
    })
}

exports.find_by_category = function (req, res) {
    // Product.findOne({
    //         categoryId: req.query.categoryId
    //     })
    //     .populate('categoryId')
    //     .exec(function (err, result) {
    //         var data = result
    //         if (err) return res.send(err)
    //         res.json({
    //             status: 200,
    //             data: data
    //         })
    //     })
    // Product.find({
    //     categoryId: req.query.categoryId
    // }, function (err, result) {
    //     if (err) return res.status(500).send('Internal Server Error')

    //     // result.categoryId = categoryId
    //     res.json(result)
    // })

    // Product.find({
    //         categoryId: req.query.categoryId
    //     })
    //     .exec(function (err, result) {
    //         if (err) return res.status(500).send('Internal Server Error')

    //         res.json(result)
    //     })
    Product.find({
            categoryId: req.query.categoryId
        })
        .populate({
            path: 'categoryId'
        })
        .exec(function (err, result) {
            if (err) return res.status(500).send('Internal Server Error')

            res.json(result)
        })
}

exports.update_by_status = function (req, res) {
    Product.findOneAndUpdate(req.query.id, req.body, {
        new: true
    }, function (err, result) {
        if (err) return res.status(500).send('Internal Server Error')

        res.json({
            status: 200,
            message: 'You have successfully update the loan status'
        })
    })
}

exports.loan_by_status = function (req, res) {
    Product.find({
            statusId: '5cd839a1a6ad4b601f58c881'
        })
        .populate({
            path: 'categoryId'
        })
        .populate({
            path: 'owner'
        })
        .exec(function (err, result) {
            if (err) return res.status(500).send('Internal Server Error')
            if (!result) return res.status(404).send('Data Not Found')

            var data = result

            res.json({
                status: 200,
                data: data
            })
        })
}

exports.loan_pending = function (req, res) {
    Product.find({
            statusId: '5cd839b8a6ad4b601f58c882'
        })
        .populate({
            path: 'categoryId'
        })
        .populate({
            path: 'owner'
        })
        .exec(function (err, result) {
            if (err) return res.status(500).send('Internal Server Error')
            if (!result) return res.status(404).send('Data Not Found')

            var data = result

            res.json({
                status: 200,
                data: data
            })
        })
}

exports.check_my_bid_turn = function (req, res) {
    Product.find({
        _id: req.query.id,
        counter: req.query.counter
    }, (err, result) => {
        if (err) return res.status(500).send('Internal Server Error')

        var data = {
            count: result.length,
            turn: false,
            message: 'Its Not Your Turn'
        }

        if (data.count != 0) {
            data.turn = true
            data.message = 'Its Your Turn'
        }

        return res.json({
            data
        })
    })
}


exports.bid_the_loan = function (req, res) {
    Loan.find({
        loanId: req.query.id
    }, (err, result) => {
        if (err) return res.status(500).send('Internal Server Error')
        if (!result) return res.status(404).send('Data Not Found')

        var data = {
            count: result.length
        }

        var now = new Date()
        Product.find({
            _id: req.query.id,
            start_event: {
                $lte: now
            },
            end_event: {
                $gte: now
            }
        }, (err, result) => {
            if (err) return res.send(err)

            var response = {
                count: result.length
            }

            if (response.count != 0) {
                Product.findOne({
                    _id: req.query.id
                }, (err, result) => {
                    if (err) return res.status(500).send('Internal Server Error')
                    var index = result.counter


                    if (index === data.count) index = 1
                    else index += 1

                    Product.findOneAndUpdate(req.query.id, {
                        winner: req.body.winner,
                        price: req.body.price,
                        counter: index
                    }, {
                        new: true
                    }, function (err, result) {
                        if (err) return res.status(500).send('Internal Server Error')

                        return res.json({
                            status: 200,
                            message: 'Cool! Your Bid Have Been Recorded'
                        })
                    })
                })
            } else {
                return res.json({
                    message: 'Its Not a Loan Time'
                })
            }
        })
    })
}

exports.loan_start_by_id = function (req, res) {
    Product.findOne({
            _id: req.query.id
        })
        .populate({
            path: 'winner',
            select: 'name'
        })
        .exec(function (err, result) {
            if (err) return res.status(500).send('Internal Server Error')
            var data = result
            return res.json({
                status: 200,
                data: data
            })
        })
}