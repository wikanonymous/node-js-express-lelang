var mongoose = require('mongoose')
var Schema = mongoose.Schema

var loanSchema = new Schema({
    loanId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Product'
    },
    userId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    counter: {
        type: Number,
        default: 0,
    }
})

module.exports = mongoose.model('Loan', loanSchema)