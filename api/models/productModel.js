var mongoose = require('mongoose')
var Schema = mongoose.Schema

var productSchema = new Schema({
    categoryId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Category'
    },
    owner: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    statusId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Status',
        default: '5cd839b8a6ad4b601f58c882'
    },
    name: {
        type: String,
        required: true
    },
    license: {
        type: String,
        default: "Tidak ada"
    },
    tax: {
        type: String,
        default: 'Mati'
    },
    created_year: {
        type: Number,
    },
    color: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    start_event: {
        type: Date,
        required: true
    },
    end_event: {
        type: Date,
        required: true
    },
    winner: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    counter: {
        type: Number,
        default: 1
    }
})

module.exports = mongoose.model('Product', productSchema)