var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    roleId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Role',
        default: '5cd83acb27b60b62c43c45f4'
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    telp: {
        type: Number,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date
    }
})

module.exports = mongoose.model('User', userSchema)