var mongoose = require('mongoose')
var Schema = mongoose.Schema

var statusSchema = new Schema({
    name: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Status', statusSchema)