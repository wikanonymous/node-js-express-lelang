var jwt = require('jsonwebtoken')
var config = require('../config')

function verify(req, res, next) {
    var token = req.headers['x-access-token']

    if (!token) res.status(403).send({
        auth: false,
        message: 'Forbidden Access'
    })

    jwt.verify(token, config.secret, function (err, decoded) {
        if (err) res.status(500).send({
            auth: false,
            message: 'Failed to Authenticate Token'
        })

        req.userId = decoded.id
        next()
    })
}

module.exports = verify