var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser')
var jwt = require('jsonwebtoken')
var bcrypt = require('bcrypt')
var config = require('../config')
var mongoose = require('mongoose')
var tokenVerify = require('./tokenVerify')
const User = require('../models/userModel')


router.use(bodyParser.urlencoded({
    extended: false
}))

router.use(bodyParser.json())

exports.register = function (req, res) {
    var hashedPassword = bcrypt.hashSync(req.body.password, 8)

    User.create({
        email: req.body.email,
        password: hashedPassword,
        name: req.body.name,
        telp: req.body.telp,
        roleId: req.body.roleId
    }, function (err, result) {
        if (err) return res.status(500).send('There was a problem registering user')
        else {
            var token = jwt.sign({
                id: result._id
            }, config.secret, {
                expiresIn: 86400
            })
            var data = result
            res.json({
                status: 200,
                auth: true,
                token: token,
                message: 'You have successfully registered'
                // data: data,
            })
        }
    })
}

exports.profile = function (req, res, next) {
    User.findById(req.userId, {
        password: 0
    }, function (err, result) {
        if (err) return res.status(500).send('There was a problem to finding profile')
        if (!result) return res.status(404).send('Data not found')
        var data = result
        res.status(200).json({
            data: data
        })
    })
}

exports.login = function (req, res) {
    User.findOne({
        email: req.body.email
    }, function (err, result) {
        if (err) return res.status(500).send('Internal Server Error')
        if (!result) return res.status(404).send('User Not Found')

        var passIsValid = bcrypt.compareSync(req.body.password, result.password)

        if (!passIsValid) return res.status(401).send({
            auth: false,
            token: null,
            message: 'Unauthorized User Password'
        })

        var token = jwt.sign({
            id: result._id
        }, config.secret, {
            expiresIn: 86400
        })
        var data = {
            roleId: result.roleId,
            _id: result._id,
            email: result.email,
            name: result.name
        }

        res.status(200).send({
            auth: true,
            token: token,
            data: data
        })
    })
}

exports.logout = function (req, res) {
    res.status(200).json({
        auth: false,
        token: null
    })
}