const express = require('express')
const router = express.Router()
var tokenVerify = require('../auth/tokenVerify')

const User = require('../controllers/userController')
const Role = require('../controllers/roleController')
const Category = require('../controllers/categoryController')
const Status = require('../controllers/statusController')
const Product = require('../controllers/productController')
const Auth = require('../auth/authController')
const Loan = require('../controllers/loanController')

router.post('/register', Auth.register)
router.post('/login', Auth.login)
router.get('/logout', Auth.logout)
router.get('/profile', tokenVerify, Auth.profile)

router.post('/user', User.create_user)
router.put('/user-update', User.update_user)
router.delete('/user', User.delete_user)
router.get('/user', User.user_by_id)
router.get('/users', User.user_list)

router.post('/role', Role.create_role)
router.get('/role', Role.role_list)
router.delete('/role/:roleId/delete', Role.delete_role)

router.post('/category', Category.create_category)
router.get('/category', Category.category_list)

router.post('/status', Status.create_status)
router.get('/status', Status.status_list)
router.put('/status/:statusId/update', Status.update_status)
router.delete('/status/:statusId/delete', Status.delete_status)

router.post('/loan', Product.create_product)
router.get('/loans', Product.list_product)
router.put('/loan', Product.update_product)
router.get('/loan', Product.find_by_category)
router.delete('/loan', Product.delete_product)
router.put('/update-loan-status', Product.update_by_status)
router.get('/loan-status', Product.loan_by_status)
router.get('/loan-check-turn', Product.check_my_bid_turn)
router.get('/bid-loan', Product.bid_the_loan)
router.get('/loan-start', Product.loan_start_by_id)
router.get('/loan-pending', Product.loan_pending)

router.post('/loan-register', Loan.loan_register)
router.get('/loan-participant', Loan.loan_list)
router.get('/loan-by-user', Loan.loan_list_by_userId)
router.get('/check-counter', Loan.check_counter)

module.exports = router