var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require('mongoose'),
    Task = require('./api/models/todoListModel'), //created model loading
    bodyParser = require('body-parser'),
    socket = require('socket.io');

const uri = "mongodb+srv://test:230998alam@cluster0-c5il3.mongodb.net/test";

let mongodb = process.env.MONGODB_URI || uri
mongoose.connect(mongodb, {
    useNewUrlParser: true
})
mongoose.Promise = global.Promise

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())

var routes = require('./api/routes/todoListRoutes') //import routes
//register routes
app.use('/api-v1', routes)

var server = app.listen(port)

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html')
})

var users = require('./api/controllers/userController')

var io = socket(server)

io.on('connection', function (socket) {
    console.log('New user from id: ' + socket.id)
    socket.on('chat message', function (msg) {
        console.log(msg)
    });
})

// var loan = io
//             .of('/')
// var user

console.log('Lelang API server started on: ' + port);